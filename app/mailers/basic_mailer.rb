# frozen_string_literal: true

# :Basic Mailer for Sending emails for basic actions:
class BasicMailer < ApplicationMailer
  def send_invite(email, test_id)
    @test = LeadershipSurvey.find(test_id)
    @sender = @test.user.name
    mail(to: email, subject: "Leadership Survey Invitation from #{@sender}")
  end

  def send_excel_user_invite(user, password)
    @user = user
    @password = password
    subject = 'Your credentionals for Leadership'
    call_mailgun_service(user, password, subject)
  rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy,
         Net::SMTPSyntaxError, Net::SMTPFatalError,
         Net::SMTPUnknownError
    mail(to: @user.email, subject: subject)
  end

  def call_mailgun_service(user, password, subject)
    Mailgun::EmailService.new(
      user.email,
      subject,
      'excel_user_invite_email',
      template_vars(user, password)
    ).send_email
  end

  def template_vars(user, password)
    {
      email: user.email,
      password: password,
      login_url: new_user_session_url
    }
  end
end
